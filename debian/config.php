<?php
// Ukolovnik configuration file for Debian
// vim: expandtab sw=4 ts=4 sts=4:

// Source dbconfig-common generate file
require ('/etc/ukolovnik/config-db.php');

// MySQL database configuration
$db_server      = $dbserver;
$db_user        = $dbuser;
$db_password    = $dbpass;
$db_database    = $dbname;
$table_prefix   = 'ukolovnik_';
?>
